/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('users', {
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
		},
		firstName: {
			type: DataTypes.STRING(50),
			allowNull: true,
		},
		lastName: {
			type: DataTypes.STRING(50),
			allowNull: true,
		},
		email: {
			type: DataTypes.STRING(100),
			allowNull: true,
			unique:true
		},
		phone: {
			type: DataTypes.STRING(12),
			allowNull: false,
		},
		techSkills: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		totalExp: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		relevantExp: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		deleteddAt: {
			type: DataTypes.DATE,
			allowNull: true,
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
		}
	}, {
		tableName: 'users'
	});
};
