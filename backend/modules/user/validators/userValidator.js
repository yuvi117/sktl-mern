const Joi = require('@hapi/joi');
const { responses } = require('../../../util');

const userValidations = {
  addUser: (req, res, next) => {
    const schema = Joi.object({
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().required().email({ minDomainSegments: 2, tlds: { allow: ["com", "net", "in"]} }),
      phone: Joi.number().required().positive().integer().min(1000000000).max(9999999999),
      techSkills: Joi.string().required(),
      totalExp: Joi.string().required(),
      relevantExp: Joi.string().required()
    });

    const validateBody = schema.validate(req.body);
    if (validateBody.error) {
      const errorMessage = validateBody.error.details[0].message;
      return responses.sendError(res, "", {}, errorMessage, 'PARAMETER_MISSING');
    }
    req.body = validateBody.value;
    next();
  },
}

module.exports = userValidations;