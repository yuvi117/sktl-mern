const router = require('express').Router();
const userController = require('../controllers/user.controller');
const userValidator = require('../modules/user/validators/userValidator');

router.get('/', userController.users);
router.get('/users', userController.users);
router.get('/users/:id', userController.user);
router.post('/add-user', userValidator.addUser, userController.addUser);

module.exports = router;