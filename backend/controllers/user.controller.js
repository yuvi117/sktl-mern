const Users = require('../models').users;

exports.users = async function(req, res, next){

  Users.findAll().then(function (users) {
    return res.send({status:200, message:"All Users", data:users});
  }).catch(function(err) {
    return res.send({status:400, message:"something went wrong"});
  });
};

exports.user = async function (req, res, next) {

  if ( req.params.id ) {
    Users.findOne({where:{ id: req.params.id }}).then(function(user) {
      return res.send({status:200, message:"User profile", data:user});
    }).catch(function(err) {
      return res.send({status:400, message:"something went wrong"});
    });
  } else {
    return res.send({status:400, message:"User Id required"});
  }
};

exports.addUser = async function(req, res, next){

  try {
    const user = await Users.create({ 
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      phone: req.body.phone,
      techSkills: req.body.techSkills,
      totalExp: req.body.totalExp,
      relevantExp: req.body.relevantExp,
    });

    res.send({status:200, message:'User Created successfully', data:user})
  } catch (err) {
    res.send({status:400, message:'Something went wrong, Please try again'})
  }
};