import React, {useState, useEffect, useCallback} from 'react';
import validate from './validator/validateForm';
import 'bootstrap/dist/css/bootstrap.min.css';
import { API_URL } from './constants/config';
import "./AddUser.css";

const AddUser = (props) => {
  const [message, setMessage] = useState("");
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  
  const validateForm = () => {
    return (values.email && values.email.length > 0)
      && (values.techSkills && values.techSkills.length > 0)
      && (values.totalExp && values.totalExp.length > 0)
      && (values.phone && values.phone.length > 0)
      && (values.firstName && values.firstName.length > 0)
      && (values.lastName && values.lastName.length > 0)
      && (values.relevantExp && values.relevantExp.length > 0);
  }

  const handleChange = (event) => {
    event.preventDefault();
    setValues({ ...values, [event.target.name]:event.target.value});
  }

  useEffect(() => {
    setErrors(validate(values));
  }, [values]);

  function handleSubmit(e){
    if (e) e.preventDefault();
    setErrors(validate(values));
    setIsSubmitting(true);
  }

  const signup = useCallback(async () => {
    let res = await fetch(`${API_URL}/api/v1/add-user`, {
      method:'POST',
      headers:{
        'Content-Type': 'application/json',
      }, 
      body: JSON.stringify(values),
    });
    
    let data = await res.json();
    setIsSubmitting(false);
    
    if ( data.status === 200 ) {
      setMessage(data.message);
      setValues({});
    } else if ( data.status === 400 ) {
      setMessage(data.message);
      setTimeout(() => { setMessage('')}, 5000);
    }
  }, [values]);

  useEffect( () => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      signup();
    } else {
      setIsSubmitting(false);
    }
  }, [errors, isSubmitting, signup]);

return <>
  <div className="App">
    <div className="auth-wrapper">
      <div className="auth-inner">

        <form onSubmit={handleSubmit} >
            <h5>Add User</h5>
            {message && (<p className="error-style">{message}</p>)}
            <div className="form-group">
              <label>First Name</label>
              <input type="text" className="form-control" name="firstName" value={values.firstName || ''} onChange={handleChange}  autoComplete="off" required/>
              {errors.firstName && ( <p className="error-style">{errors.firstName}</p>)}
            </div>

            <div className="form-group">
              <label>Last Name</label>
              <input type="text" className="form-control" name="lastName" value={values.lastName || ''} onChange={handleChange}  autoComplete="off" required/>
              {errors.lastName && ( <p className="error-style">{errors.lastName}</p>)}
            </div>

            <div className="form-group">
              <label>Email address</label>
              <input type="text" className="form-control" name="email" value={values.email || ''} autoComplete="off" onChange={handleChange} required/>
              {errors.email && ( <p className="error-style">{errors.email}</p>)}
            </div>

            <div className="form-group">
              <label>Phone</label>
              <input type="text" className="form-control" name="phone" value={values.phone || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.phone && ( <p className="error-style">{errors.phone}</p>)}
            </div>

            <div className="form-group">
              <label>Technical Skills</label>
              <input type="text" className="form-control" name="techSkills" value={values.techSkills || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.techSkills && (<p className="error-style">{errors.techSkills}</p>)}
            </div>

            <div className="form-group">
              <label>Total Experience</label>
              <input type="text" className="form-control" name="totalExp" value={values.totalExp || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.totalExp && ( <p className="error-style">{errors.totalExp}</p>)}
            </div>

            <div className="form-group">
              <label>Relevant Experience </label>
              <input type="text" className="form-control" name="relevantExp" value={values.relevantExp || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.relevantExp && ( <p className="error-style">{errors.relevantExp}</p>)}
            </div>

            <button type="submit" className="btn btn-primary btn-block" disabled={!validateForm()}>Submit </button>
        </form>
      </div>
    </div>
  </div>
  </>;
}

export default AddUser;