export default function validate(values) {
  let errors = {};
  if (values.firstName && !/^[a-zA-Z\s]{1,100}$/.test(values.firstName)) {
    errors.firstName = 'First name must be upto 100 alphabets allowed';
  }
  if (values.lastName && !/^[a-zA-Z\s]{1,100}$/.test(values.lastName)) {
    errors.lastName = 'Last name must be must be upto 100 alphabets allowed';
  }
  if (values.email && !/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = 'Email address is invalid';
  }
  if (values.relevantExp && !/^[a-z\d\-_\s]{1,255}$/.test(values.relevantExp)) {
    errors.relevantExp = 'Relevant Experience must be between 6 or 255 characters';
  }
  if (values.totalExp && !/^[a-z\d\-_\s]{1,255}$/.test(values.totalExp)) {
    errors.totalExp = 'Total Experience must be between 6 or 255 characters';
  }
  if (values.phone && !/^[0-9]{10}$/.test(values.phone)) {
    errors.phone = 'Phone must be number 10 digits';
  }
  if (values.techSkills && !/^[a-z\d\-_\s]{2,255}$/.test(values.techSkills)) {
    errors.techSkills = 'Technical Skills must be between 6 or 255 characters';
  }
  return errors;
};
  